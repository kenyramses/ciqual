/*jslint sloppy:true */
/*globals Routing, $, windows, GLOBAL_JS_STRING, alert, console */

$(document).ready(function(){

    var Manager = (function () {
        'use strict';

        return {
            datatable: null,

             createParcel : function (event) {
                 return $.ajax({
                     type: "POST",
                     url: Routing.generate('create_parcel')
                 }).done(function (data) {
                     $('#div_container_parcel_form').append(data);
                 })
                     .fail(function () {
                     });
             }
        };
    }());

    // $(document).on('click', '.new_intervention_button', function(event){
    //
    //      $('#modal_create_intervention').modal({
    //         show: true
    //      });
    //
    //      $('#currentRequestId').text($(event.target).attr('data-RequestId'));
    //
    //      event.stopImmediatePropagation();
    //      return false;
    //  });
    $(document).on('click', '#button_create_parcel_form', function (event) {
        Manager.createParcel(event);
    })
    

});