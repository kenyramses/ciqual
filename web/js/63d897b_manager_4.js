/*jslint sloppy:true */
/*globals Routing, $, windows, GLOBAL_JS_STRING, alert, console */

$(document).ready(function(){

    // var Manager = (function () {
    //     'use strict';
    //
    //     return {
    //         datatable: null,
    //
    //         init: function () {
    //             Manager.datatable = $('#IngredientTab').DataTable({
    //                 "paging": true,
    //                 "retrieve": true,
    //                 "lengthChange": false,
    //                 "bInfo": false,
    //                 "bFilter": false,
    //                 "oLanguage": {
    //                     "sEmptyTable": GLOBAL_JS_STRING.datatablesEmptyTable,
    //                     "oPaginate": {
    //                         "sNext": GLOBAL_JS_STRING.datatableNext,
    //                         "sPrevious": GLOBAL_JS_STRING.datatablePrevious
    //                     }
    //                 }
    //             });
    //         }
    //     };
    // }());

    $('#inputsearch').autocomplete({
        source : function(requete, reponse) {
            $.ajax({
                url: Routing.generate('ingrediens_match_list'),
                dataType: 'json',
                data: {criteria:  $('#inputsearch').val()},
                success: function (data) {
                    var el = data;
                    reponse($.map(el,
                        function (el) {
                            return {
                                value: el.ingredient
                            }
                        }));
                }
            });
        }
        // ,select: function (event, ui) {
        //     $('#textarea-content').val(ui.item.value);
        //     event.stopPropagation();
        // }
    });



});