Test FoodMeUp
========================

Ceci est mon implémentation du test Ciqual

Pour voir le resultat :

----------------------


Merci d'exécuter les commandes suivantes:

  * composer install

  * php bin/console assets:install

  * php bin/console assetic:dump
  
  * php bin/console doctrine:database:create //to create te database && execute migrations

  * php bin/console doctrine:fixtures:load
  
  
  NB:
  Je n'ai pas pu importer le fichier csv au format mysql, afin de ne pas être bloqué j'ai du créer une table Food contenant des donnés
  similaire à ceux dans le fichier Table_Ciqual_2016.csv .
  
  * J'ai pensé à créer une commande pour charger les données depuis le fichier et de les enregistrer via doctrine en bd.
