<?php

namespace FrontOfficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use function var_dump;

class HomeController extends Controller
{
    /**
     * @Route("/", name="search_ingredient")
     */
    public function indexAction()
    {
        return $this->render('FrontOfficeBundle:Search:search.html.twig');
    }

    /**
     * @Route("xhr-auto-complete-search", name="ingrediens_match_list", options={"expose"= true})
     *
     * @param Request $request
     */
    public function xhrSearchIngrediens(Request $request)
    {
        return new Response($this->get('FrontOfficeBundle\Services\createClient')->getIngredientsCriteria($request->get('criteria')));
    }

    /**
     * @Route("list-nutrients", name="list_nutrients")
     */
    public function listNutrientsAction(Request $request)
    {
        $contents = $this->get('FrontOfficeBundle\Services\createClient')->getFoods($request->get('_inputsearch'));

        return $this->render('FrontOfficeBundle:Search:list_nutrients.html.twig',
            [
                'pagination' => $this->get('knp_paginator')->paginate($contents, $request->query->get('page', 1), 10),
            ]
        );
    }
}
