<?php

namespace FrontOfficeBundle\Services;

use ApiBundle\Entity\Food;
use GuzzleHttp\Client as GuzzleClient;
//use JMS\Serializer\Serializer;
use function json_decode;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Router;
use Symfony\Component\Serializer\Serializer;
use const true;
use function var_dump;

class CreateClient
{
    /**
     * @var $baseUri string
     */
    private $baseUri;

    /** @var Router */
    private $router;

    private $serializer;

    /**
     * CreateClient constructor.
     * @param Router $router
     * @param $baseUri
     * @param Serializer $serializer
     */
    public function __construct(Router $router, $baseUri, Serializer $serializer)
    {
        $this->baseUri = $baseUri;
        $this->router = $router;
        $this->serializer = $serializer;
    }

    /**
     * mixed data
     */
    public function getIngredientsCriteria($criteria)
    {
        $client = $this->instantiateClient();

        $contents = $client->request('GET',  $this->router->generate('ingrediens_criteria_list',['criteria'=>$criteria]));

        return $contents->getBody()->getContents();
    }

    /**
     * @param $criteria
     * @return string
     */
    public function getFoods($criteria)
    {
        $client = $this->instantiateClient();

        $contents = $client->request('GET', $this->router->generate('api_nutrients_list', ['criteria' => $criteria]));

        return json_decode($contents->getBody()->getContents(), true);
    }

    private function instantiateClient()
    {
        return new GuzzleClient(['base_uri' => $this->baseUri]);
    }

}