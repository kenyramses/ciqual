/*jslint sloppy:true */
/*globals Routing, $, windows, GLOBAL_JS_STRING, alert, console */

$(document).ready(function(){

    $('#inputsearch').autocomplete({
        source : function(requete, reponse) {
            $.ajax({
                url: Routing.generate('ingrediens_match_list'),
                dataType: 'json',
                data: {criteria:  $('#inputsearch').val()},
                success: function (data) {
                    var el = data;
                    reponse($.map(el,
                        function (el) {
                            return {
                                value: el.ingredient
                            }
                        }));
                }
            });
        }
    });



});