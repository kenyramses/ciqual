<?php

namespace ApiBundle\Controller;

use ApiBundle\Entity\Food;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View as RestView;


class IngredientController extends FOSRestController
{
    /**
     * @Rest\Get("/ingrediens/{criteria}", name="ingrediens_criteria_list", options={ "method_prefix" = false }))
     */
    public function getIngredientMatchingCriteriaAction(Request $request, $criteria)
    {
        return $this->getDoctrine()->getManager()->getRepository(Food::class)->findByCriteria($criteria);
    }


    /**
     * @Rest\Get("/nutrients/{criteria}", name="api_nutrients_list", options={ "method_prefix" = false }))
     */
    public function getNutrientsAction(Request $request, $criteria)
    {
        return $this->getDoctrine()->getManager()->getRepository(Food::class)->findBy(['ingredient' => $criteria]);
    }

}
