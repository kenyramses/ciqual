<?php

namespace ApiBundle\Controller;

use ApiBundle\Entity\Phone;
use ApiBundle\Entity\User;
use ApiBundle\Form\PhoneType;
use ApiBundle\Form\UserType;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\View\View as RestView;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class PhoneBookController
 * @package ApiBundle\Controller
 */
class PhoneController extends FOSRestController
{

    /**
     * @Rest\Get("/phones", name="phones_list", options={"method_prefix" = false})
     *
     * @param Request $request
     */
    public function getPhonesAction(Request $request)
    {
        if (!$phones = $this->getDoctrine()->getRepository(PhoneType::class)->findAll()) {
            return $this->PhoneNotFound();
        }

        return $phones;
    }

    /**
     * @Rest\Get("/user/{id}/phones", name="get_specific_user_phones", options={"methode_prefix" = false})
     * @param Request $request
     */
    public function getPhoneAction(Request $request)
    {
        if (!$user = $this->getDoctrine()->getRepository(UserType::class)->find($request->get('id'))) {
            return $this->UserNotFound();
        }

        /* @var $user User */
        return $user->getPhones();
    }

    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post("/user/{id}/phones", name="post_number", options={"method_prefix"= false})
     *
     * @param Request $request
     */
    public function postPhoneAction(Request $request)
    {
        if (!$user = $this->getDoctrine()->getRepository(UserType::class)->find($request->get('id'))) {
            return $this->UserNotFound();
        }

        $phone = new Phone();
        $form = $this->createForm(PhoneType::class, $phone);
        $form->handleRequest($request);

       $form->submit($request->request->all());

       if ($form->isSubmitted() && $form->isValid()) {
           $em = $this->getDoctrine()->getManager();
           $em->persist($phone);
           $em->flush();

           return$phone;
       } else {
            return $form;
       }

    }

    public function deletePhoneAction()
    {
    }

    private function PhoneNotFound()
    {
        return RestView::create(['message'=> 'Phone not found'], Response::HTTP_NOT_FOUND );
    }

    private function UserNotFound()
    {
        return RestView::create(['message'=> 'User not found'], Response::HTTP_NOT_FOUND );
    }

}