<?php

namespace ApiBundle\Controller;

use ApiBundle\Entity\User;
use ApiBundle\Form\UserType;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View as RestView;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class PhoneBookController
 * @package ApiBundle\Controller
 */
class UserController extends FOSRestController
{
    /**
     * @Rest\Get("/users", name="users_list", options={"method_prefix" = false})
     *
     * @param Request $request
     */
    public function getUsersAction(Request $request)
    {
        if (!$users = $this->getDoctrine()->getRepository(UserType::class)->findAll()) {
            return $this->UserNotFound();
        }

        return $users;
    }

    /**
     * @Rest\Get("/user/{id}", name="get_single_user", options={"methode_prefix" = false})
     * @param Request $request
     */
    public function getUserAction(Request $request)
    {
        if (!$user = $this->getDoctrine()->getRepository(UserType::class)->find($request->get('id'))) {
            return $this->UserNotFound();
        }

        return $user;
    }

    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post("/user/{id}", name="post_user", options={"method_prefix"= false})
     *
     * @param Request $request
     */
    public function postUserAction(Request $request)
    {
        if (!$user = $this->getDoctrine()->getRepository(UserType::class)->find($request->get('id'))) {
            return $this->UserNotFound();
        }

        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        $form->submit($request->request->all());

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $user;
        } else {
            return $form;
        }

    }

    public function updateUserAction(Request $request)
    {
        if (!$user = $this->getDoctrine()->getRepository(UserType::class)->find($request->get('id'))) {
            return $this->UserNotFound();
        }

        /* @var $user User */

        $form = $this->createForm(UserType::class, $user);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->merge($user);
            $em->flush();

            return $user;

        } else {
            return $form;
        }
    }

    /**
     * @Rest\View(statusCode= Response::HTTP_NO_CONTENT)
     * @Rest\Delete("/user/{id}", name="remove_user_by_id", options={"method_prefix"= false})
     * @param Request $request
     */
    public function removeUserAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        if (!$user = $this->getDoctrine()->getRepository(UserType::class)->find($request->get('id'))) {
            return $this->UserNotFound();
        }
        /* @var $user User */

        foreach ($user->getPhones() as $phone) {
            $em->remove($phone);
        }

        $em->remove($user);

    }

    private function findUser(Request $request)
    {
        if (!$user = $this->getDoctrine()->getRepository(UserType::class)->find($request->get('id'))) {
            return $this->UserNotFound();
        }
        return $user;
    }

    private function PhoneNotFound()
    {
        return RestView::create(['message'=> 'Phone not found'], Response::HTTP_NOT_FOUND );
    }

    private function UserNotFound()
    {
        return RestView::create(['message'=> 'User not found'], Response::HTTP_NOT_FOUND );
    }


}