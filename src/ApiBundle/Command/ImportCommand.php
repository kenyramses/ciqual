<?php

namespace ApiBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportCommand extends ContainerAwareCommand
{
    public function configure()
    {
       $this
           ->setName('import:data:from:csv')
           ->setDescription('import data from csv file to a Mysql database')
       ;
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {

    }

    /**
     * This method will import csv data from a cvs file to mysql
     */
    protected function importCsvIntoMysql()
    {

    }

}
