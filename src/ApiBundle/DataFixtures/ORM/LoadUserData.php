<?php

namespace ApiBundle\DataFixtures\ORM;

use ApiBundle\Entity\Food;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadUserData implements FixtureInterface
{public function load(ObjectManager $manager)
{
    foreach ($this->getArticles() as $data) {
        $food = new Food();
        foreach ($data as $fieldName => $fieldValue) {
            $food->{'set'.ucfirst($fieldName)}($fieldValue);
        }
        $manager->persist($food);
    }
    $manager->flush();
}
    private function getArticles()
    {
        return [
            [
                'ingredient' => 'Farines et amidons',
                'composition' => 'Fécule de pomme de terre',
                'energy' => 'cru',
                'weight' => '12',
            ],
            [
                'ingredient' => 'Riz et graines crus',
                'composition' => 'Blé de Khorasan',
                'energy' => '124',
                'weight' => '13',
            ],
            [
                'ingredient' => 'Farines et amidons',
                'composition' => 'Farine de blé tendre ou froment T110',
                'energy' => 'cru',
                'weight' => '50',
            ],
            [
                'ingredient' => 'Farines et amidons',
                'composition' => 'Farine de riz',
                'energy' => 'cru',
                'weight' => '12',
            ],

            [
                'ingredient' => 'Farines et amidons',
                'composition' => 'Farine de sarrasin',
                'energy' => 'cru',
                'weight' => '12',
            ],
            [
                'ingredient' => 'Riz et graines crus',
                'composition' => 'Blé de Khorasan',
                'energy' => '124',
                'weight' => '13',
            ],
            [
                'ingredient' => 'Farines et amidons',
                'composition' => 'Farine de sarrasin',
                'energy' => 'cru',
                'weight' => '50',
            ],
            [
                'ingredient' => 'Farines et amidons',
                'composition' => 'Farine de blé tendre ou froment T150',
                'energy' => 'cru',
                'weight' => '12',
            ],
            [
                'ingredient' => 'Farines et amidons',
                'composition' => 'Farine de raisin',
                'energy' => 'cru',
                'weight' => '50',
            ],
            [
                'ingredient' => 'Farines et amidons',
                'composition' => 'Farine de sarrasin',
                'energy' => 'cru',
                'weight' => '12',
            ],
            [
                'ingredient' => 'Riz et graines crus',
                'composition' => 'Blé de djzdz',
                'energy' => '124',
                'weight' => '13',
            ],
            [
                'ingredient' => 'Farines et amidons',
                'composition' => 'Farine de dzdzd',
                'energy' => 'cru',
                'weight' => '50',
            ],
            [
                'ingredient' => 'Farines et amidons',
                'composition' => 'Farine de blé tendre',
                'energy' => 'cru',
                'weight' => '12',
            ],
            [
                'ingredient' => 'Farines et amidons',
                'composition' => 'Farine de viarrasin',
                'energy' => 'cru',
                'weight' => '50',
            ],
            [
                'ingredient' => 'Farines et amidons',
                'composition' => 'Farine de blé T150',
                'energy' => 'cru',
                'weight' => '12',
            ],

            [
                'ingredient' => 'Riz et graines crus',
                'composition' => 'Blé de Khorasan',
                'energy' => '124',
                'weight' => '13',
            ],
            [
                'ingredient' => 'Farines et amidons',
                'composition' => 'Farine de seigle T110',
                'energy' => 'cru',
                'weight' => '50',
            ],
            [
                'ingredient' => 'Riz et graines crus',
                'composition' => 'Farine de milet',
                'energy' => '1546',
                'weight' => '65',
            ],
            [
                'ingredient' => 'Pâtes et semoules crues',
                'composition' => 'Polenta ou semoule de mais.',
                'energy' => 'précuite',
                'weight' => '7',
            ],
            [
                'ingredient' => 'Pâtes et semoules crues',
                'composition' => 'Vermicelle de riz',
                'energy' => 'sans gluten',
                'weight' => '10',
            ],
            [
                'ingredient' => 'Riz et graines cuits',
                'composition' => 'kdkdz.',
                'energy' => 'riz blanc',
                'weight' => '58',
            ],
            [
                'ingredient' => 'Riz et graines cuits',
                'composition' => 'sqjdkbc.',
                'energy' => 'Riz rouge',
                'weight' => '85',
            ],
            [
                'ingredient' => 'Pate et semoules cuites',
                'composition' => 'Nouilles asiatiques cuites',
                'energy' => 'cuit',
                'weight' => '30',
            ],
            [
                'ingredient' => 'Pains',
                'composition' => 'Pain',
                'energy' => 'Baguette',
                'weight' => '42',
            ],
            [
                'ingredient' => 'Pains speciaux',
                'composition' => 'azertyu.',
                'energy' => 'courant',
                'weight' => '25',
            ],
            [
                'ingredient' => 'Biscottes et pains grillé',
                'composition' => 'Muffins Anglais',
                'energy' => 'tranches',
                'weight' => '14',
            ],
            [
                'ingredient' => 'Viennoiseries et brioches',
                'composition' => 'Croissant aux amandes',
                'energy' => 'sans fourage',
                'weight' => '95',
            ],
            [
                'ingredient' => 'Gâteaux et patisserie',
                'composition' => 'Gressin',
                'energy' => 'type de mirroir',
                'weight' => '61',
            ],
            [
                'ingredient' => 'Biscuits sec et sucrés',
                'composition' => 'Biscuits secs pauvres en glucide.',
                'energy' => 'Baba au rhum',
                'weight' => '25',
            ],
            [
                'ingredient' => 'Feuillés et cake salés',
                'composition' => 'chips de mais',
                'energy' => 'base de pommes de terre',
                'weight' => '30',
            ],
            [
                'ingredient' => 'Pâte et tarte',
                'composition' => 'ddkbdksd.',
                'energy' => 'pizza',
                'weight' => '62',
            ],
            [
                'ingredient' => 'Barres céréalisées',
                'composition' => 'Un duo de choc.',
                'energy' => 'crackers',
                'weight' => '65',
            ],
            [
                'ingredient' => 'Céréales petit déjeuner chocolaté',
                'composition' => 'pétales de blé chocolat',
                'energy' => 'Fourrage au chocolat',
                'weight' => '01',
            ],

            [
                'ingredient' => 'Biscuits sec et sucrés',
                'composition' => 'Biscuits secs.',
                'energy' => 'rhum',
                'weight' => '25',
            ],
            [
                'ingredient' => 'Feuillés et cake salés',
                'composition' => 'chips de mais',
                'energy' => 'base de pommes',
                'weight' => '30',
            ],
            [
                'ingredient' => 'Pâte et tarte',
                'composition' => 'ddbsdksc.',
                'energy' => 'pizza',
                'weight' => '62',
            ],
            [
                'ingredient' => 'Barres céréalisées',
                'composition' => 'ddnzkdz.',
                'energy' => 'crackers sec',
                'weight' => '65',
            ],
            [
                'ingredient' => 'Barres céréalisées',
                'composition' => 'dbcbssc.',
                'energy' => 'crackers sec',
                'weight' => '65',
            ],
            [
                'ingredient' => 'Barres céréalisées',
                'composition' => 'Un duo de choc.',
                'energy' => 'crackers',
                'weight' => '65',
            ],
            [
                'ingredient' => 'Barres céréalisées',
                'composition' => 'zkdznz.',
                'energy' => 'crackers sec',
                'weight' => '65',
            ],
            [
                'ingredient' => 'Céréales petit déjeuner chocolaté',
                'composition' => 'pétales de blé chocolat',
                'energy' => 'chocolat',
                'weight' => '01',
            ],
        ];
    }

}